require 'test_helper'

class TweetTest < ActiveSupport::TestCase
  test 'should not save tweet without message' do
    tweet = Tweet.new
    assert_not tweet.save
  end

  test 'should not save tweet with only space in message' do
    tweet = Tweet.new(message: ' ')
    assert_not tweet.save
  end

  test 'should not save tweet with 141 characters in message' do
    tweet_message = 'this line is consisted of 40 characters ' \
    'this line is consisted of 40 characters ' \
    'this line is consisted of 40 characters ' \
    '21 chars in this line'

    tweet = Tweet.new(message: tweet_message)
    assert_not tweet.save
  end
end
